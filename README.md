# Like & Dislike

This module provides "like" and "dislike" widgets for contents inside Drupal,
making it easiers to promote features as the one seem on many social network
websites.

Technically speaking, the module provides 2 tags for Voting API, "like" and
"dislike", working in a different way from
[Vote Up/Down](https://www.drupal.org/project/vote_up_down),
that is like a "plus or minus" approach. Likes are separate from Dislikes here.

For a full description of the module, visit the
[Like & Dislike](https://www.drupal.org/project/like_and_dislike).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/like_and_dislike).


## Table of Contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module does not have any dependency on any other module.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Like/Dislike widget works for any Entity Types and Bundles, and includes a
settings page to specify on which bundles the user wants the widgets available
(node types, comments, files, users, etc). By default, none of them are
available, so it is needed to go at "/admin/config/search/like_and_dislike"
and enable the wanted ones.


## Maintainers

- Pedro Rocha - [pedrorocha](https://www.drupal.org/u/pedrorocha)
- Adrian Cid Almaguer - [adriancid](https://www.drupal.org/u/adriancid)
- Sascha Grossenbacher - [Berdir](https://www.drupal.org/u/berdir)
